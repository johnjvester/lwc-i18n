import { LightningElement, track } from 'lwc';
import welcomeHeader from '@salesforce/label/c.Welcome_Header';
import welcomeText from '@salesforce/label/c.Welcome_Text';
import labelName from '@salesforce/label/c.Label_Name';
import placeholderName from '@salesforce/label/c.Placeholder_Name';
import labelAmountDue from '@salesforce/label/c.Label_Amount_Due';
import placeholderAmountDue from '@salesforce/label/c.Placeholder_Amount_Due';
import labelDueDate from '@salesforce/label/c.Label_Due_Date';
import placeholderDueDate from '@salesforce/label/c.Placeholder_Due_Date';
import labelSeason from '@salesforce/label/c.Label_Season';
import placeholderSeason from '@salesforce/label/c.Placeholder_Season';
import valueSpring from '@salesforce/label/c.Value_Spring';
import valueSummer from '@salesforce/label/c.Value_Summer';
import valueFall from '@salesforce/label/c.Value_Fall';
import valueWinter from '@salesforce/label/c.Value_Winter';

export default class I18nExample extends LightningElement {

    label = {
        welcomeHeader,
        welcomeText,
        labelName,
        labelAmountDue,
        labelDueDate,
        labelSeason
    };

    placeholder = {
        placeholderName,
        placeholderAmountDue,
        placeholderDueDate,
        placeholderSeason
    };

    option = {
        valueSpring,
        valueSummer,
        valueFall,
        valueWinter
    };

    name;
    amountDue;
    dueDate;
    season;

    seasons = [
        {value: "1", label: valueSpring},
        {value: "2", label: valueSummer},
        {value: "3", label: valueFall},
        {value: "4", label: valueWinter},
    ];
}